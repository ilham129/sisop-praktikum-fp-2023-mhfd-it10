# sisop-praktikum-fp-2023-MHFD-IT10


# Database

 __Analisa Soal__


Pada soal disuruh membuat progam database menggunakan bahasa c, dalam progam database tersebut harus bisa membuat database, membuat table, menghapus database, menghapus tabel, menghapus kolom, menambahkan data pada kolom, mengubah data pada kolom, menghapus data pada kolom, dan memilih kolom.


__Code Progam__

- **Membuat Tabel**
```
void MembuatTable(int sock, char *database_Name_, char *table_Name_, char *_Columns) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);
    char Database_Location[512];
    snprintf(Database_Location, sizeof(Database_Location), "databases/%s", database_Name_);

    struct stat st;
    if (stat(Database_Location, &st) != 0 || !S_ISDIR(st.st_mode)) {
        printf("Database '%s' not found.\n", database_Name_);
        send(sock, "Database not found", strlen("Database not found"), 0);
    }

    FILE *file = fopen(File_Location, "w");
    if (file == NULL) {
        printf("Failed to create table '%s'.\n", table_Name_);
        return;
    }

    for (int i = 0; table_Name_[i] != '\0'; i++) {
        if (!isalnum(table_Name_[i])) {
            printf("Invalid table name.\n");
            send(sock, "Invalid table name", strlen("Invalid table name"), 0);
            fclose(file);
            return;
        }
    }

    int len = strlen(_Columns);
    _Columns[len - 2] = '\0';

    fprintf(file, "%s\n", _Columns);

    fclose(file);

    printf("Table '%s' was created successfully in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "Table was created successfully", strlen("Table was created successfully"), 0);
}

```
1. Fungsi **`MembuatTable`** menerima argumen untuk membuat tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel dan lokasi database.
3. Fungsi **`stat`** digunakan untuk memeriksa ketersediaan direktori database.
4. Fungsi **`fopen`** digunakan untuk membuka file tabel dengan mode "w" (write).
5. Dilakukan pengecekan karakter-karakter dalam nama tabel, jika tidak valid, maka akan dicetak pesan error.
6. Panjang daftar kolom dihitung dan formatnya disesuaikan.
7. Daftar kolom ditulis ke dalam file tabel menggunakan **`fprintf`**.
8. File tabel ditutup menggunakan **`fclose`**.
9. Pesan sukses dicetak dan pesan sukses dikirimkan ke soket.

- **Menghapus tabel**
```
void MenghapusTable(int sock, char *database_Name_, char *table_Name_) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }

    int status = remove(File_Location);

    if (status == 0) {
        printf("Table '%s' was successfully removed from database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table was successfully removed", strlen("Table was successfully removed"), 0);
    } else {
        printf("Failed to delete table '%s' from database '%s'.\n", table_Name_, database_Name_);
    }
}
```
1. Fungsi **`MenghapusTable`** menerima argumen untuk menghapus sebuah tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. Fungsi **`remove`** digunakan untuk menghapus file tabel.
5. Jika penghapusan berhasil, dicetak pesan sukses.
6. Jika penghapusan gagal, dicetak pesan error.

- **Menghapus kolom**
```
void MenghapusKolom(int sock, char *database_Name_, char *table_Name_, char *Column__Name) {
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }
    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }
    char line_[500];
    fgets(line_, sizeof(line_), file);

    int _Index__Column = -1;
    char *__token = strtok(line_, ",");
    int _Index = 0;
    while (__token != NULL) {
        if (strcmp(__token, Column__Name) == 0) {
            _Index__Column = _Index;
            break;
        }
        _Index++;
        __token = strtok(NULL, ",");
    }

    fclose(file);

    if (_Index__Column == -1) {
        printf("Column '%s' not found in table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
        send(sock, "Column not found", strlen("Column not found"), 0);
        return;
    }

    char File_Temporary_Location[512];
    snprintf(File_Temporary_Location, sizeof(File_Temporary_Location), "databases/%s/temp.csv", database_Name_);
    FILE *File_Temporary = fopen(File_Temporary_Location, "w");
    if (File_Temporary == NULL) {
        printf("Failed to create temporary file.\n");
        return;
    }

    file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        fclose(File_Temporary);
        remove(File_Temporary_Location);
        return;
    }

    while (fgets(line_, sizeof(line_), file)) {
        char *__token = strtok(line_, ",");
        int columnCount = 0;
        while (__token != NULL) {
            columnCount++;
            if (columnCount != _Index__Column + 1) {
                fprintf(File_Temporary, "%s,", __token);
            }
            __token = strtok(NULL, ",");
        }
        fprintf(File_Temporary, "\n");
    }

    fclose(file);
    fclose(File_Temporary);

    remove(File_Location);

    rename(File_Temporary_Location, File_Location);

    printf("Column '%s' was successfully deleted from table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
    send(sock, "Column was successfully deleted", strlen("Column was successfully deleted"), 0);
}

```
1. Fungsi **`MenghapusKolom`** menerima argumen untuk menghapus sebuah kolom dalam sebuah tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "r" (read) menggunakan **`fopen`**.
5. Baris pertama file tabel dibaca dan disimpan dalam variabel.
6. Kolom yang ingin dihapus diidentifikasi berdasarkan nama kolom yang cocok dengan baris pertama.
7. File tabel ditutup menggunakan **`fclose`**.
8. Jika kolom tidak ditemukan, dicetak pesan error.
9. File temporary baru dibuat.
10. File tabel dibuka kembali dan file temporary dibuka. Jika gagal, dicetak pesan error.
11. Baris-baris file tabel diproses, kolom yang ingin dihapus tidak ditulis ke file temporary.
12. Setelah semua baris selesai diproses, file tabel dan file temporary ditutup.
13. File tabel asli dihapus dan file temporary diubah namanya menjadi file tabel asli.
14. Pesan sukses dicetak dan pesan sukses dikirimkan ke soket.

- **Menambah data**
```
void MenambahData(int sock, char *database_Name_, char *table_Name_, char *values) {

    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }

    FILE *file = fopen(File_Location, "a");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char *__token = strtok(values, ",");
    char line_[500] = "";
    bool Value1 = true;

    while (__token != NULL) {
       
        while (*__token == ' ' || *__token == '(' || *__token == '\'')
            __token++;
        size_t len = strlen(__token);
        while (len > 0 && (__token[len - 1] == ' ' || __token[len - 1] == ')' || __token[len - 1] == '\''))
            __token[--len] = '\0';

    
        if (Value1) {
            Value1 = false;
            snprintf(line_ + strlen(line_), sizeof(line_) - strlen(line_), "%s", __token);
        } else {
            snprintf(line_ + strlen(line_), sizeof(line_) - strlen(line_), ",%s", __token);
        }

        __token = strtok(NULL, ",");
    }

    fprintf(file, "%s\n", line_);
    fclose(file);

    printf("Data added successfully to table '%s' in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "Data added successfully to table", strlen("Data added successfully to table"), 0);
}
```
1. Fungsi **`MenambahData`** menerima argumen untuk menambahkan data ke dalam sebuah tabel dalam sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "a" (append) menggunakan **`fopen`**.
5. Data yang akan ditambahkan diproses dalam loop untuk menghilangkan karakter yang tidak diinginkan.
6. Setiap token data yang telah diproses ditambahkan ke variabel, dipisahkan oleh tanda koma.
7. Data yang akan ditambahkan ditulis ke file tabel menggunakan **`fprintf`**.
8. File tabel ditutup menggunakan **`fclose`**.
9. Pesan sukses dicetak.

- **Mengubah data**
```
void MengubahData(int sock, char *database_Name_, char *table_Name_, char *Column__Name, char *New__Data) {
   
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);


    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }


    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char line_[500];
    fgets(line_, sizeof(line_), file);

    int _Index__Column = -1;
    char *__token = strtok(line_, ",");
    int _Index = 0;
    while (__token != NULL) {
        if (strcmp(__token, Column__Name) == 0) {
            _Index__Column = _Index;
            break;
        }
        _Index++;
        __token = strtok(NULL, ",");
    }

    fclose(file);

    if (_Index__Column == -1) {
        printf("Column '%s' not found in table '%s' in database '%s'.\n", Column__Name, table_Name_, database_Name_);
        send(sock, "Column not found", strlen("Column not found"), 0);
        return;
    }

    char File_Temporary_Location[512];
    snprintf(File_Temporary_Location, sizeof(File_Temporary_Location), "databases/%s/temp.csv", database_Name_);
    FILE *File_Temporary = fopen(File_Temporary_Location, "w");
    if (File_Temporary == NULL) {
        printf("Failed to create temporary file.\n");
        return;
    }

    file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        fclose(File_Temporary);
        remove(File_Temporary_Location);
        return;
    }

    fgets(line_, sizeof(line_), file);
    fprintf(File_Temporary, "%s", line_);

    while (fgets(line_, sizeof(line_), file)) {
        char *__token = strtok(line_, ",");
        int columnCount = 0;
        while (__token != NULL) {
            columnCount++;
            if (columnCount == _Index__Column + 1) {
                fprintf(File_Temporary, "%s,", New__Data);
            } else {
                fprintf(File_Temporary, "%s,", __token);
            }
            __token = strtok(NULL, ",");
        }
        fprintf(File_Temporary, "\n");
    }

    fclose(file);
    fclose(File_Temporary);


    remove(File_Location);

    rename(File_Temporary_Location, File_Location);

    printf("Column '%s' in table '%s' in database '%s' was successfully updated.\n", Column__Name, table_Name_, database_Name_);
    send(sock, "Column was successfully updated", strlen("Column was successfully updated"), 0);
}
```
1. Fungsi **`MengubahData`** menerima argumen untuk mengubah data dalam sebuah kolom dalam tabel sebuah database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "r" (read) menggunakan **`fopen`**.
5. Baris pertama file tabel dibaca untuk mendapatkan nama-nama kolom.
6. Kolom yang ingin diubah dicari dalam baris pertama tersebut.
7. File tabel ditutup.
8. Jika kolom tidak ditemukan, dicetak pesan error.
9. File temporary dibuat untuk menyimpan hasil perubahan.
10. File tabel dibuka kembali.
11. Baris pertama file tabel ditulis ke file temporary.
12. Setiap baris dari file tabel dibaca dan diproses.
13. Setiap token dalam baris diproses. Jika di kolom yang ingin diubah, data baru ditulis ke file temporary. Jika tidak, token tersebut ditulis kembali.
14. Setelah semua token dalam baris selesai diproses, baris tersebut ditulis ke file temporary.
15. Setelah semua baris selesai diproses, file tabel dan file temporary ditutup.
16. File tabel asli dihapus.
17. File temporary diubah namanya menjadi nama file tabel asli.
18. Pesan sukses dicetak.

- **Menghapus data**
```
void HapusData(int sock, char *database_Name_, char *table_Name_) {
  
    char File_Location[512];
    snprintf(File_Location, sizeof(File_Location), "databases/%s/%s.csv", database_Name_, table_Name_);

    if (access(File_Location, F_OK) == -1) {
        printf("Table '%s' not found in database '%s'.\n", table_Name_, database_Name_);
        send(sock, "Table not found", strlen("Table not found"), 0);
        return;
    }


    FILE *file = fopen(File_Location, "r");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    char line_[500];
    fgets(line_, sizeof(line_), file);
    
    fclose(file);

    file = fopen(File_Location, "w");
    if (file == NULL) {
        printf("Failed to open table '%s'.\n", table_Name_);
        return;
    }

    fprintf(file, "%s", line_);
    
    fclose(file);

    printf("All rows have been deleted from table '%s' in database '%s'.\n", table_Name_, database_Name_);
    send(sock, "All rows have been deleted from table", strlen("All rows have been deleted from table"), 0);
}

```
1. Fungsi **`HapusData`** menerima argumen untuk menghapus semua data dalam sebuah tabel dalam database.
2. Variabel lokal digunakan untuk menyimpan lokasi file tabel.
3. Fungsi **`access`** digunakan untuk memeriksa ketersediaan file tabel, jika tidak ditemukan maka dicetak pesan error.
4. File tabel dibuka dalam mode "r" (read) menggunakan **`fopen`**.
5. Baris pertama file tabel dibaca untuk mendapatkan nama-nama kolom.
6. File tabel ditutup.
7. File tabel dibuka kembali dalam mode "w" (write) menggunakan **`fopen`**.
8. Baris pertama (nama-nama kolom) ditulis kembali ke file tabel.
9. File tabel ditutup kembali.
10. Pesan sukses dicetak.

__Kendala__

Belum selesai DML dan susah untuk membuat progam yang melacak file csv.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/elvarabumbungan/sisop-praktikum-fp-2023-mhfd-it10.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/elvarabumbungan/sisop-praktikum-fp-2023-mhfd-it10/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
